const assert = require("assert");
const axios = require("axios");
const app = require("../src/app");

const port = app.get("port") || 3030;
const hostname = app.get("hostname") || "localhost";
const client = axios.create({
  baseURL: `http://${hostname}:${port}/`,
});

describe("Feathers application tests", () => {
  beforeAll(done => {
    this.server = app.listen(port);
    this.server.once("listening", () => done());
  });

  afterAll(done => {
    this.server.close(() => {
      app.primus.end();
      done();
    });
  });

  describe("404", () => {
    test("shows a 404 JSON error without stack trace", () =>
      client.get("path/to/nowhere").catch(err => {
        assert.equal(err.response.status, 404);
        assert.equal(err.response.data.message, "Page not found");
        assert.equal(err.response.data.name, "NotFound");
      }));
  });
});
